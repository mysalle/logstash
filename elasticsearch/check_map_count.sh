#!/bin/bash

# https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html

check_vm_map_count() {
  if [[ ! "$(sysctl vm.max_map_count)" =~ "262144" ]] ; then
    echo 'first execute:'
    echo sudo sysctl -w vm.max_map_count=262144
    if ! sudo sysctl -w vm.max_map_count=262144 ; then
      exit 1
    fi
  fi
}

check_vm_map_count
