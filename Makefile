all: test

up: build
	docker-compose \
		--project-name moodle \
		--file src/main/docker-compose/docker-compose.yml \
		up

build:
	docker-compose \
		--file src/main/docker-compose/docker-compose.yml \
		build

test: buildTest
	-rm -r build/test
	mkdir -p build/test/docker-bind
	docker run \
		--rm \
		--mount "type=bind,source=${PWD}/build/test/docker-bind,target=/usr/share/logstash/output/" \
		docker.elastic.co/logstash/logstash-test
	bash -c "diff -u --color=always \
		<(cat build/test/docker-bind/my.json | jq -S 'del(.[\"host\"])') \
		src/test/resources/expected.json"

buildTest:
	docker build \
		--file src/test/docker/Dockerfile \
		--tag docker.elastic.co/logstash/logstash-test \
		src/test


.PHONY: all up build test buildTest
